# Rust-wm
A work in progress tiling window manager for windows written in rust.

## Setup
Requires that windows explorer be killed in order to properly bind most shortcuts.

Start the program with

```
cargo run
```

Cargo should automatically pull all needed prerequisites.

There is currently no method in place to prevent multiple concurrent instances and there is no
tray icon to indicate the program is running. It is not currently recommended to run the program
outside of a shell.

## Keyboard shortcuts

| Shortcut        | Action                                                         |
| --------------- | -------------------------------------------------------------- |
| Win+w           | Add an existing window (unfloat)                               |
| Win+s           | Swap the current frame between horizontal and vertical layouts |
| Win+a           | Surround the selected window with a new frame                  |
| Win+r           | Use pmenu to start a new program                               |
| Win+t           | Use pmenu to prompt for a window by name to raise              |
| Win+h           | Select the window to the left                                  |
| Win+j           | Select the window beneath                                      |
| Win+k           | Select the window above                                        |
| Win+l/;         | Select the window to the right (l is likely broken)            |
| Win+H           | Move the selected window to the left                           |
| Win+J           | Move the selected window down                                  |
| Win+K           | Move the selected window up                                    |
| Win+[0-9]       | Select the last used window on the nth workspace               |
| Win+Shift+[0-9] | Move the current window to the nth workspace                   |
| Win+L           | Move the selected window to the right                          |
| Win+f           | Print debug information about the current window state         |
| Win+x           | Close the current window                                       |
| Win+c           | Clean closed windows from management                           |
| Win+C           | Clear all currently managed windows                            |
| Win+q           | Quit the program                                               |

