use std::process::Command;
use std::env;
use std::path::Path;
use std::fs::File;
use std::io::Write;
fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();
    let profile = env::var("PROFILE").unwrap();
    let cmenu_project = Path::new(&out_dir).join("cmenu.pro");
    let mut f = File::create(&cmenu_project).unwrap();
    //probably need this to be c string
    let base_path = Path::new(".").canonicalize().unwrap();
    let p = base_path.to_str().unwrap().as_bytes();
    let mut pro: Vec<u8> = Vec::new();
    pro.extend(b"TEMPLATE = lib\nSOURCES += ");
    //need to investigate why shaving of 4 characters is needed
    pro.extend(&p[4..]);
    pro.extend(b"\\cmenu\\cmenu.cpp");
    pro.extend(b"\nHEADERS += ");
    pro.extend(&p[4..]);
    pro.extend(b"\\cmenu\\cmenu.h");
    pro.extend(b"\nQT += widgets\n");
    f.write_all(&pro).unwrap();
    f.sync_all().unwrap();
    let _sd = env::current_dir().unwrap();
    env::set_current_dir(&out_dir).unwrap();
    if !Command::new("qmake.exe")
        .env("CD",&out_dir)
        .arg("cmenu.pro")
        .status()
        .unwrap()
        .success() {
            panic!("qmake failed")
        }
    if !if profile == "debug" {
        println!(r"cargo:rustc-link-search={}\debug\",out_dir);
        Command::new("nmake.exe")
            .arg("debug")
            .status()
            .unwrap()
    } else if profile == "release" {
        println!(r"cargo:rustc-link-search={}\release\",out_dir);
        Command::new("nmake.exe")
            .arg("release")
            .status()
            .unwrap()
    } else {
        panic!("unknown build profile");
    }.success() {
        panic!("nmake failed")
    }
    //set back original dir. Doesn't seem to be needed
    //env::set_current_dir(_sd).unwrap();
}
