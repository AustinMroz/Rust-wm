#ifdef __cplusplus
#include <QMainWindow>
#include <QLabel>
#include <QString>

class Cmenu : public QMainWindow {
   public:
      Cmenu(const char** choices, size_t count, QMainWindow* parent);
      size_t getChoice();
   protected:
      virtual void keyPressEvent(QKeyEvent *event);
   private:
      QString input;
      QLabel* label;
      size_t selected;
      const char** choices;
      size_t count;
      void updateInput();
};
int isMatch(const char* a, const char* b);
std::vector<size_t> pick(const char** choices, size_t count, const char* input);
extern "C" {
#endif
 __declspec(dllexport) uint32_t choose_string(const char** choices, uint32_t count,
                                              uint32_t x, uint32_t y, uint32_t w);
#ifdef __cplusplus
}
#endif
