#include "cmenu.h"
#include <QApplication>
#include <QLabel>
#include <QFont>
#include <QMainWindow>
#include <QKeyEvent>
#include <iostream>
#include <QString>
#include <vector>
#include <queue>

#include <filesystem>
#include <windows.h>

Cmenu::Cmenu(const char** choices, size_t count, QMainWindow* parent)
   : QMainWindow(parent) {
      this->choices = choices;
      this->count = count;
      input = "";
      selected = 0;
      label = new QLabel("");
      QFont font(label->font());
      font.setPointSize(25);
      label->setFont(font);
      this->updateInput();
      this->setCentralWidget(label);
   }

void Cmenu::keyPressEvent(QKeyEvent* event){
   if(event->key() == Qt::Key_Return) {
      close();
   } else if(event->key() == Qt::Key_Escape) {
      selected = count;
      close();
      return;
   } else if (event->key() == Qt::Key_Backspace) {
      if(input.length() > 0){
         input.remove(input.length()-1,1);
      }
   } else {
      input+=event->text();
   }
   updateInput();
}

void Cmenu::updateInput() {
   QString disp;
   disp+=input;
   disp+= " | ";
   std::string str = input.toStdString();
   auto rank = pick(choices, count, str.c_str());
   if (!rank.empty())
      selected = rank.front();
   for(size_t i : rank) {
      disp+=choices[i];
      disp+=", ";
   }
   label->setText(disp);
}

size_t Cmenu::getChoice() {
   return selected;
}

uint32_t choose_string(const char** choices, uint32_t count,
                       uint32_t x, uint32_t y, uint32_t w) {
   int argc = 1;
   struct {char one; char two;} str = {'a','\0'};
   char *a = (char *)&str;
   QApplication app(argc, &a);
   Cmenu window(choices, count, 0);
   window.resize(w,10);
   window.move(x,y);
   window.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
   window.show();
   window.activateWindow();
   while(!window.isActiveWindow()) {
      Sleep(100);
      window.activateWindow();
   }

   app.exec();
   return (uint32_t)window.getChoice();
}
std::vector<size_t> pick(const char** choices, size_t count, const char* input) {
   using namespace std;
   auto cmp = [](pair<int, size_t> l, pair<int, size_t> r)
      {return l.first < r.first;};
   priority_queue<pair<int, size_t>,std::vector<pair<int, size_t>>,
                  decltype(cmp)> pq(cmp);
   for(size_t i=0;i<count;i++) {
      const char* choice = choices[i];
      int match = isMatch(input, choice);
      if(match != 0) {
         pq.push(make_pair(match+8, i));
         continue;
      }
      const char* cp = choice+1;
      while(*cp != NULL) {
         int match = isMatch(input, cp);
         if(match != 0) {
            pq.push(make_pair(match, i));
            break;
         }
         cp++;
      }
   }
   vector<size_t> res;
   while(!pq.empty()) {
      res.push_back(pq.top().second);
      pq.pop();
   }
   return res;
}
/**
 * check to what extent a is a match of b
 * 0 if no match
 * 1 if ignore_case partial
 * 2 if ignore_case complete
 * 3 if partial
 * 4 if complete
 * TODO: switch to smart case?
 */
int isMatch(const char* a, const char* b) {
   bool case_match = true;
   char ca;
   char cb;
   while((ca = *a) != NULL && (cb = *b) != NULL) {
      if(ca != cb) {
         //Note that this is incorrect in the case both ca and cb are lower case
         //But this doesn't matter as they would already match then
         if(ca >= 'a' && ca <= 'z')
            ca-= 'a'-'A';
         else if (cb >= 'a' && cb <= 'z')
            cb-= 'a'-'A';
         if(ca == cb)
            case_match = false;
         else
            break;
      }
      a++;
      b++;
   }
   if(*a != NULL)
      return 0;
   return 1+case_match*2 + (*b == NULL)*4;
}
