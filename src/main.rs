extern crate winapi;

use winapi::shared::windef;
use winapi::um::errhandlingapi;
use winapi::shared::minwindef;
use winapi::um::winuser;
use winapi::um::shellapi;
use std::sync::mpsc;
use std::collections::BTreeMap;

//mod dialogue;

fn main() {
    ensure_single_instance();
    let (sender, receiver) = mpsc::channel();
    std::thread::spawn(move || {WM::new(receiver).process_events();});
    unsafe {
        use std::ptr::null_mut;
        let mut msg = winuser::MSG {hwnd: 0 as windef::HWND, message: 0 as minwindef::UINT,
        wParam: 0 as minwindef::WPARAM, lParam: 0 as minwindef::LPARAM,
        time: 0 as minwindef::DWORD, pt: windef::POINT{x: 0,y: 0,},};

        let mut hm: BTreeMap<isize, fn(&mut WM)> = BTreeMap::new();
        let mut id = 1;
        {
            let mut ahk = |k: u16, m: u16, f: fn(&mut WM)| {
                add_hotkey(k, m, &mut hm, &mut id, f);
            };
            //can't roll these into a for loop
            //since that creates a closure instead of a fn
            ahk(72,    8, |wm: &mut WM| wm.select(&Direction::Left));
            ahk(74,    8, |wm: &mut WM| wm.select(&Direction::Down));
            ahk(75,    8, |wm: &mut WM| wm.select(&Direction::Up));
            ahk(76,    8, |wm: &mut WM| wm.select(&Direction::Right));
            ahk(0xBA,  8, |wm: &mut WM| wm.select(&Direction::Right));
            ahk(72,   12, |wm: &mut WM| wm.move_generic(&Direction::Left));
            ahk(74,   12, |wm: &mut WM| wm.move_generic(&Direction::Down));
            ahk(75,   12, |wm: &mut WM| wm.move_generic(&Direction::Up));
            ahk(76,   12, |wm: &mut WM| wm.move_generic(&Direction::Right));

            ahk(0x57,  8, WM::add_active_window);
            ahk(0x57, 12, WM::play_video);
            ahk(70,    8, WM::print);
            ahk(83,    8, WM::swap_active_frame);
            ahk(83,   12, WM::make_frame_rigid);
            ahk(65,    8, WM::add_parent_frame);
            ahk(88,    8, WM::close_active_window);
            ahk(0x54,  8, WM::prompt_window);
            ahk(0x52,  8, WM::start_program);
            ahk(0x44,  8, |_: &mut WM| toggle_decorations());
            ahk(0x43,  8, WM::clear_dead);
            ahk(0x43, 12, WM::clear_state);

            ahk(49,    8, |wm: &mut WM| wm.select_workspace(0));
            ahk(50,    8, |wm: &mut WM| wm.select_workspace(1));
            ahk(51,    8, |wm: &mut WM| wm.select_workspace(2));
            ahk(52,    8, |wm: &mut WM| wm.select_workspace(3));
            ahk(53,    8, |wm: &mut WM| wm.select_workspace(4));
            ahk(54,    8, |wm: &mut WM| wm.select_workspace(5));
            ahk(55,    8, |wm: &mut WM| wm.select_workspace(6));
            ahk(56,    8, |wm: &mut WM| wm.select_workspace(7));
            ahk(57,    8, |wm: &mut WM| wm.select_workspace(8));
            ahk(58,    8, |wm: &mut WM| wm.select_workspace(9));
            ahk(49,   12, |wm: &mut WM| wm.move_to_workspace(0));
            ahk(50,   12, |wm: &mut WM| wm.move_to_workspace(1));
            ahk(51,   12, |wm: &mut WM| wm.move_to_workspace(2));
            ahk(52,   12, |wm: &mut WM| wm.move_to_workspace(3));
            ahk(53,   12, |wm: &mut WM| wm.move_to_workspace(4));
            ahk(54,   12, |wm: &mut WM| wm.move_to_workspace(5));
            ahk(55,   12, |wm: &mut WM| wm.move_to_workspace(6));
            ahk(56,   12, |wm: &mut WM| wm.move_to_workspace(7));
            ahk(57,   12, |wm: &mut WM| wm.move_to_workspace(8));
            ahk(58,   12, |wm: &mut WM| wm.move_to_workspace(9));
            ahk(49,   10, |wm: &mut WM| wm.workspace_only(0));
            ahk(50,   10, |wm: &mut WM| wm.workspace_only(1));
            ahk(51,   10, |wm: &mut WM| wm.workspace_only(2));
            ahk(52,   10, |wm: &mut WM| wm.workspace_only(3));
            ahk(53,   10, |wm: &mut WM| wm.workspace_only(4));
            ahk(54,   10, |wm: &mut WM| wm.workspace_only(5));
            ahk(55,   10, |wm: &mut WM| wm.workspace_only(6));
            ahk(56,   10, |wm: &mut WM| wm.workspace_only(7));
            ahk(57,   10, |wm: &mut WM| wm.workspace_only(8));
            ahk(58,   10, |wm: &mut WM| wm.workspace_only(9));
        }
        register_hotkey(8, 81, &mut id);

        while winuser::GetMessageW(&mut msg, null_mut(),0,0) != 0 {
            if msg.message == winuser::WM_HOTKEY {
                if let Err(err) = match hm.get(&msg.lParam) {
                    Some(s) => sender.send(*s),
                    None => {
                        if msg.lParam == ((81<<16)|8) {
                            std::process::exit(0);
                        }
                        Ok(())
                    }
                } {
                    panic!("Failed to send action function due to {:?}", err);
                }
            } else {
                winuser::TranslateMessage(&msg);
                winuser::DispatchMessageW(&msg);
            }
        }
    }
}
fn add_hotkey(key: u16, modif: u16, hm: &mut BTreeMap<isize, fn(&mut WM)>, id: &mut i32, fun: fn(&mut WM)) {
    let k: isize = ((key as isize)<<16)|(modif as isize);
    hm.insert(k,fun);
    register_hotkey(modif as isize, key.into(), id);
}
fn register_hotkey(mods: isize, vk: u32, id: &mut i32) {
    use std::ptr::null_mut;
    unsafe {
        if winuser::RegisterHotKey(null_mut(), *id, mods as u32, vk.into()) == 0 {
            println!("Failed to register {},{} as hotkey:{}", vk, mods, errhandlingapi::GetLastError());
        }
        *id+=1;
    }
}

struct WM {
    workspaces: Vec<Frame>,
    displays: Vec<(windef::RECT, usize)>,
    rx: mpsc::Receiver<fn(&mut WM)>,
}
impl WM {
    //TODO:: Clean up member functions so only those dependent on self are contained
    fn new(rx: mpsc::Receiver<fn(&mut WM)>) -> WM {
        let mut disp: Vec<(windef::RECT, usize)> = Vec::new();
        let mut w: Vec<Frame> = Vec::new();
        let mut i = 0;
        for m in list_monitors() {
            disp.push((m,i));
            w.push(Frame {layout: Layout::Horizontal, children: Vec::new(), lsi: 0, area: m, preserve_area: false});
            i+=1;
        }
        //in case windows are mistakenly resized offscreen,
        //give them a proper area to resize to
        let null_area = windef::RECT {top: -1080-20, left: 0, bottom: 0, right: 1920};
        while i < 10 {
            w.push(Frame {layout: Layout::Horizontal, children: Vec::new(), lsi: 0, area: null_area, preserve_area: false});
            i+=1;
        }
        WM {workspaces: w, displays: disp, rx: rx}
    }
    /**
     * Print the current state of the window manager
     * This needs to be a fairly manual process since I can't implement format
     * for types outside of this crate (winapi), and can't cleanly wrap Vec's
     */
    fn print(&mut self) {
        print!("[");
        for (r, s) in &self.displays {
            print!("({},{},{},{}):{},",r.left,r.top,r.right,r.bottom,s);
        }
        //try to backspace the last comma
        print!("]->");
        println!("{:?}", self.workspaces);

    }
    fn add_active_window(&mut self) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        if let Some(_indexes) = self.find_parent(hwnd) {
            //window was already added
            return;
        }
        let mut area = windef::RECT {top: 0, bottom: 0, left: 0, right: 0};
        unsafe {winuser::GetWindowRect(hwnd, &mut area);}
        let mi = self.determine_main_monitor(area);
        add_window(hwnd, &mut self.workspaces[self.displays[mi].1]);
    }
    fn close_active_window(&mut self) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        if let Some(indexes) = self.find_parent(hwnd) {
            let mut f: &mut Frame = &mut self.workspaces[indexes[0]];
            WM::remove_walk(&indexes[1..], f);
            //resize_frame(f,f.area)
        } else {
            println!("Window was not found for deletion");
        }
        unsafe {winuser::PostMessageA(hwnd, winuser::WM_CLOSE, 0,0);}
    }

    fn add_parent_frame(&mut self) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        let mut area = windef::RECT {top: 0, bottom: 0, left: 0, right: 0};
        unsafe {winuser::GetWindowRect(hwnd, &mut area);}
        if let Some(indexes) = self.find_parent(hwnd) {
            let mut f: &mut Frame = &mut self.workspaces[indexes[0]];
            WM::add_parent_walk(&indexes[1..], f, area);
        }
    }
    fn add_parent_walk(path: &[usize], frame: &mut Frame, area: windef::RECT) {
        if path.len() == 1 {
            let c = frame.children.remove(path[0]);
            let mut newf = Frame {layout: Layout::Horizontal, children: Vec::new(),
                                    lsi: 0, area: area, preserve_area: false};
            newf.children.push(c);
            frame.children.insert(path[0],Child::Frame(newf));
        } else {
            if let Child::Frame(ref mut f) = frame.children[path[0]] {
                WM::add_parent_walk(&path[1..], f, area);
            }
        }
    }
    /*
     * Rewrite of select operation.
     * Selecting a frame selects the most recently selected child of said frame
     * Needs refactoring to update the parent frames new selection
     */
    fn select(&mut self, dir: &Direction) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        let mut i: usize;
        let mut nsi: Option<usize> = None;
        if let Some(path) = self.find_parent(hwnd) {
            {//empty block to ensure release of f
                let mut v: Vec<&Frame> = Vec::new();
                let mut f: &Frame = &self.workspaces[path[0]];
                v.push(f);
                for i in 1..path.len()-1 {
                    f = f.children[path[i]].a_f();
                    v.push(f);
                }
                i = path.len();
                loop {
                    i -= 1;
                    let f = match v.pop() {
                        Some(fr) => fr,
                        None => break,
                    };
                    match f.layout {
                        Layout::Horizontal => match dir {
                            Direction::Up | Direction::Down => (),
                            Direction::Left => if path[i] as i32 - 1 >= 0 {
                                f.children[path[i]-1].select();
                                nsi = Some(path[i]-1);
                                break;
                            }
                            Direction::Right => if path[i]+1 < f.children.len() {
                                f.children[path[i]+1].select();
                                nsi = Some(path[i]+1);
                                break;
                            }
                        }
                        Layout::Vertical => match dir {
                            Direction::Left | Direction::Right => (),
                            Direction::Up => if path[i] as i32 - 1 >= 0 {
                                f.children[path[i]-1].select();
                                nsi = Some(path[i]-1);
                                break;
                            }
                            Direction::Down => if path[i]+1 < f.children.len() {
                                f.children[path[i]+1].select();
                                nsi = Some(path[i]+1);
                                break;
                            }
                        }
                    }
                }
            }
            if let Some(index) = nsi {
                //TODO::Update selected frame
                //i has been saved
                //step down to i-1 frame, this time passing a mutable reference
                self.workspaces[path[0]].update_lsi(&path[1..i],index)
            } else {//check for relative monitor
                if let Some(i) = self.relative_monitor(path[0],dir) {
                    self.workspaces[i].select();
                }
            }
        } else {
            let f = &self.workspaces[0];
            f.select();
        }
    }
    /*
     * In progress revision to new window selection
     * A selection event can be divided into 3 cases
     * A move within a Frame
     * A move into a Frame, or a move out of a frame
     * In the case of two adjacent frames, require two moves
     * Otherwise, it would be impossible to create window/frame between 2
     * existing frames
     *
     * This also greatly simplifies the possible cases:
     * A returned Child can be added immediately at path[0]/path[0]+1
     * depending on if it's left/up or right/down
     */
    fn move_walk(path: &[usize], frame: &mut Frame, dir: &Direction) -> Option<Child> {
        if path.len() == 1 {
            match frame.layout {
                Layout::Horizontal => {
                    match dir {
                        Direction::Up | Direction::Down =>
                            return Some(frame.children.remove(path[0])),
                        Direction::Left => {
                            let sloc = path[0] as i32 - 1;
                            if sloc < 0 {
                                //try to move left into a higher frame
                                println!("moving up");
                                return Some(frame.children.remove(path[0]));
                            } else {
                                if let Child::Frame(_) = frame.children[sloc as usize] {
                                    let w = frame.children.remove(path[0]);
                                    if let Child::Frame(ref mut sf) = frame.children[sloc as usize] {
                                        sf.children.push(w);
                                    }
                                } else {
                                    frame.children.swap(path[0],sloc as usize);
                                }
                            }
                            resize_frame(frame.area,frame);
                            return None;
                        }
                        Direction::Right => {
                            let sloc = path[0] + 1;
                            if sloc >= frame.children.len() {
                                //try to move right into higher frame
                                println!("moving up");
                                return Some(frame.children.remove(path[0]))
                            } else {
                                if let Child::Frame(_) = frame.children[sloc] {
                                    let w = frame.children.remove(path[0]);
                                    //this moves the index
                                    if let Child::Frame(ref mut sf) = frame.children[path[0]] {
                                        sf.children.push(w);
                                    }
                                } else {
                                    frame.children.swap(path[0],sloc);
                                }
                            }
                        }
                    }
                    resize_frame(frame.area,frame);
                    return None;
                },
                Layout::Vertical => {
                    match dir {
                        Direction::Left | Direction::Right =>
                            return Some(frame.children.remove(path[0])),
                        Direction::Up => {
                            let sloc = path[0] as i32 - 1;
                            if sloc < 0 {
                                //try to move up into a higher frame
                                println!("moving up");
                                return Some(frame.children.remove(path[0]));
                            } else {
                                if let Child::Frame(_) = frame.children[sloc as usize] {
                                    let w = frame.children.remove(path[0]);
                                    if let Child::Frame(ref mut sf) = frame.children[sloc as usize] {
                                        sf.children.push(w);
                                    }
                                } else {
                                    frame.children.swap(path[0],sloc as usize);
                                }
                            }
                            resize_frame(frame.area,frame);
                            return None;
                        }
                        Direction::Down => {
                            let sloc = path[0] + 1;
                            if sloc >= frame.children.len() {
                                //try to move down into higher frame
                                println!("moving up");
                                return Some(frame.children.remove(path[0]))
                            } else {
                                if let Child::Frame(_) = frame.children[sloc] {
                                    let w = frame.children.remove(path[0]);
                                    //this moves the index
                                    if let Child::Frame(ref mut sf) = frame.children[path[0]] {
                                        sf.children.push(w);
                                    }
                                } else {
                                    frame.children.swap(path[0],sloc);
                                }
                            }
                        }
                    }
                    resize_frame(frame.area,frame);
                    return None;
                },
            }
        }
        let mut moved_window: Option<Child> = None;
        if let Child::Frame(ref mut f) = frame.children[path[0]] {
            moved_window = WM::move_walk(&path[1..], f, dir);
        }
        if let Some(c) = moved_window {
            //On highest index, movements occur relative to workspace index
            //TODO::Fix highest level moves to be relative to monitor
            let ip = if frame.children[path[0]].a_f().children.len() == 0 {
                frame.children.remove(path[0]);
                path[0]
            } else {
                match dir {
                    Direction::Up | Direction::Left => path[0],
                    Direction::Down | Direction::Right => path[0]+1,
                }
            };
            frame.children.insert(ip,c);
            resize_frame(frame.area, frame);
        }
        None
    }
    fn move_generic(&mut self, dir: &Direction) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        if let Some(path) = self.find_parent(hwnd) {
            if let Some(c) = WM::move_walk(&path[1..], &mut self.workspaces[path[0]], dir) {
                //Window was moved out.
                //Either swap to the other monitor, or just re add.
                if let Some(nm) = self.relative_monitor(path[0], dir) {
                    println!("Moving from monitor {} to {}", path[0], nm);
                    match dir {
                        Direction::Left | Direction::Up =>
                            self.workspaces[nm].children.push(c),
                        Direction::Right | Direction::Down =>
                            self.workspaces[nm].children.insert(0,c),
                    }
                    resize_frame(self.workspaces[path[0]].area, &mut self.workspaces[path[0]]);
                    resize_frame(self.workspaces[nm].area, &mut self.workspaces[nm]);
                } else {
                    match dir {
                        Direction::Left | Direction::Up =>
                            self.workspaces[path[0]].children.insert(0,c),
                        Direction::Right | Direction::Down =>
                            self.workspaces[path[0]].children.push(c),
                    }
                }
            }
        }
    }
    fn remove_walk(path: &[usize], frame: &mut Frame) -> bool {
        if path.len() == 1 {
            frame.children.remove(path[0]);
            resize_frame(frame.area,frame);
        } else {
            if if let Child::Frame(ref mut f) = frame.children[path[0]] {
                WM::remove_walk(&path[1..], f)
            } else {
                println!("{:?}", path);
                unreachable!("Given path was incorrect")
            } {
                frame.children.remove(path[0]);
                resize_frame(frame.area,frame);
            }
        }
        frame.children.len() == 0
    }
    fn swap_active_frame(&mut self) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        if let Some(indexes) = self.find_parent(hwnd) {
            WM::change_layout_walk(&indexes[1..], &mut self.workspaces[indexes[0]]);
        }
    }
    fn change_layout_walk(path: &[usize], frame: &mut Frame) {
        if path.len() == 1 {
            frame.layout = match frame.layout {
                Layout::Vertical => Layout::Horizontal,
                Layout::Horizontal => Layout::Vertical,
            };
            resize_frame(frame.area,frame);
        } else {
            if let Child::Frame(ref mut f) = frame.children[path[0]] {
                WM::change_layout_walk(&path[1..], f);
            }
        }
    }
    fn make_frame_rigid(&mut self) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        if let Some(indexes) = self.find_parent(hwnd) {
            WM::make_rigid_walk(&indexes[1..], &mut self.workspaces[indexes[0]]);
            let mut f = &mut self.workspaces[indexes[0]];
            resize_frame(f.area, &mut f);
        }
    }
    fn make_rigid_walk(path: &[usize], frame: &mut Frame) {
        if path.len() == 1 {
            frame.preserve_area = true;
            let h = frame.area.bottom-frame.area.top+1;
            let w = (h*1)/2;
            frame.area =  windef::RECT {top: 0, bottom: h, left: 0, right: w as i32};
        } else {
            if let Child::Frame(ref mut f) = frame.children[path[0]] {
                WM::make_rigid_walk(&path[1..], f);
            }
        }
    }
    //Continues to process events until an error occurs
    //Does not return
    fn process_events(&mut self) {
        while let Ok(msg) = self.rx.recv() {
            msg(self);
        }
    }
    fn find_parent(&self, win: windef::HWND) -> Option<Vec<usize>> {
        //TODO:: implement in a manor that is not O(n)
        //Current implementation will use a depth first search so tracing can be done in place
        let mut res: Vec<usize> = Vec::new();
        let mut area = windef::RECT {top: 0, bottom: 0, left: 0, right: 0};
        unsafe {winuser::GetWindowRect(win, &mut area)};
        let wi = self.displays[self.determine_main_monitor(area)].1;
        res.push(wi);
        let mut f: &Frame = &self.workspaces[wi];
        let mut i: usize = 0;
        while i < f.children.len() {
            match &f.children[i] {
                Child::Window(w) => if *w == win {
                    res.push(i);
                    return Some(res);
                } else {
                    i+=1;
                    while i>= f.children.len() {
                        //slowly pop back up
                        if res.len() == 0 {
                            break;
                        }
                        f = &self.workspaces[res[0]];
                        for ind in 1..res.len()-1 {
                            f = match f.children[res[ind]] {
                                Child::Frame(ref frame) => frame,
                                Child::Window(_) => unreachable!(),
                            }
                        }
                        i = res.pop().unwrap()+1;
                    }
                }
                Child::Frame(ref sf) => {
                    res.push(i);
                    f = sf;
                    i = 0;
                }
            }
        }
        //should only get here in extenuating case.
        //most likely cause is window being closed.
        None
    }
    //fn get_frame_from_index(&mut self, index: &Vec<u32>) -> &mut Frame {

    //}
    //determines which monitor displays the most of a given window
    fn determine_main_monitor(&self, window: windef::RECT) -> usize {
        use std::cmp::{min, max};
        let mut g_area = -1;
        let mut g_monitor: usize = 0;
        for i in 0..self.displays.len() {
            let m = &self.displays[i].0;
            let left = max(m.left, window.left);
            let top = max(m.top, window.top);
            let right = min(m.right, window.right);
            let bottom = min(m.bottom, window.bottom);
            let area = (right-left)*(bottom-top);
            if area>g_area {
                g_area = area;
                g_monitor = i;
            }
        }
        g_monitor
    }
    fn determine_active_monitor(&self) -> usize {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        let mut area = windef::RECT {top:0, bottom: 0, left: 0, right: 0};
        unsafe {winuser::GetWindowRect(hwnd, &mut area)};
        return self.determine_main_monitor(area);
    }
    fn start_program(&mut self) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        let mut area = windef::RECT {top: 0, bottom: 0, left: 0, right: 0};
        unsafe {winuser::GetWindowRect(hwnd, &mut area)};
        let mi = self.determine_main_monitor(area);
        let x = self.displays[mi].0.left;
        let y = self.displays[mi].0.top;
        let w = self.displays[mi].0.right-x;
        let (names, paths) = list_programs();

        use std::ffi::CString;
        use std::os::raw::c_char;
        use std::ptr::null_mut;
        let mut owned_names: Vec<CString> = Vec::new();
        let mut choices: Vec<*const c_char> = Vec::new();
        for name in names {
            let s = CString::new(name).unwrap();
            choices.push(s.as_ptr());
            owned_names.push(s);
        }
        let i = unsafe {choose_string(choices.as_ptr(), choices.len() as u32,
                                      x, y, w)};
        if i < paths.len() as u32 {
            let p: CString = CString::new(paths[i as usize].to_str().unwrap())
                .unwrap();
            let operation = CString::new("open").unwrap();
            unsafe {shellapi::ShellExecuteA(null_mut(), operation.as_ptr(),
                                    p.as_ptr(), null_mut(), null_mut(), 1);}
        }
    }
    /**
     * Another utility function that is completely outside the scope of the
     * project
     * TODO: instead run user customizable script with clipboard as argument/ev
     */
    fn play_video(&mut self) {
        use std::ptr::null_mut;
        use std::process::Command;
        use winapi::um::winbase;
        use std::ffi::CStr;
        unsafe {
            if winuser::IsClipboardFormatAvailable(winuser::CF_TEXT) == 0 ||
                winuser::OpenClipboard(null_mut()) == 0 {
                    println!("Could not get clipboard text");
                    return;
                }
            let handle = winuser::GetClipboardData(winuser::CF_TEXT);
            if !handle.is_null() {
                let string = winbase::GlobalLock(handle) as *mut i8;
                if !string.is_null() {
                    let rstring = CStr::from_ptr(string);
                    //println!("{:?}",rstring);
                    match Command::new("mpv")
                        .arg(rstring.to_str().unwrap())
                        .spawn() {
                            Err(e) => println!("{:?}",e),
                            Ok(_) => (),
                        };
                    winbase::GlobalUnlock(handle);
                }
            }
            winuser::CloseClipboard();
        }
    }
    fn relative_monitor(&self, initial: usize, dir: &Direction) -> Option<usize> {
        //a bit of care must be taken here.
        //It's ok not to look up initial monitor,
        //but you must make certain you only iterate over monitors,
        //and can not return the one you started with
        let mut iw: Option<usize> = None;
        for i in 0..self.displays.len() {
            if self.displays[i].1 == initial {
                iw = Some(i);
            }
        }
        let initial: usize = match iw {
            Some(s) => s,
            None => return None,
        };
        let dist = u32::max_value();
        let mut  index: Option<usize> = None;
        let a1 = self.workspaces[initial].area;
        let m1x = (a1.left+a1.right)/2;
        let m1y = (a1.top+a1.bottom)/2;
        for i in 0..self.displays.len() {
            if self.displays[i].1 == initial {
                continue;
            }
            let a2 = self.workspaces[self.displays[i].1].area;
            let m2x = (a2.left+a2.right)/2;
            let m2y = (a2.top+a2.bottom)/2;
            let d: u32 = match dir {
                Direction::Up => {
                    if m2y > m1y {
                        u32::max_value()
                    } else {
                        ((a1.top-a2.bottom).abs() +
                            (m1x-m2x).abs()) as u32
                    }
                }
                Direction::Down => {
                    if m2y < m1y {
                        u32::max_value()
                    } else {
                        ((a1.bottom-a2.top).abs() +
                            (m1x-m2x).abs()) as u32
                    }
                }
                Direction::Left => {
                    if m2x > m1x {
                        u32::max_value()
                    } else {
                        ((a1.left-a2.right).abs() +
                            (m1y-m2y).abs()) as u32
                    }
                }
                Direction::Right => {
                    if m2x < m1x {
                        u32::max_value()
                    } else {
                        ((a1.right-a2.left).abs() +
                            (m1y-m2y).abs()) as u32
                    }
                }
            };
            if d < dist {
                index = Some(i);
            }
        }
        index
    }
    fn prompt_window(&mut self) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        let mut area = windef::RECT {top: 0, bottom: 0, left: 0, right: 0};
        unsafe {winuser::GetWindowRect(hwnd, &mut area)};
        let mi = self.determine_main_monitor(area);
        let x = self.displays[mi].0.left;
        let y = self.displays[mi].0.top;
        let w = self.displays[mi].0.right-x;
        let (names, handles) = list_windows();
        //Perform the fairly painful operation of converting to JSON
        use std::ffi::CString;
        use std::os::raw::c_char;
        let mut owned_names: Vec<CString> = Vec::new();
        let mut choices: Vec<*const c_char> = Vec::new();
        for name in &names {
            let s = CString::new(String::from_utf16(name).unwrap()).unwrap();
            choices.push(s.as_ptr());
            owned_names.push(s);
        }
        let i = unsafe {choose_string(choices.as_ptr(), choices.len() as u32,
                                      x, y, w)};
        if i < handles.len() as u32 {
            activate_window(handles[i as usize]);
        }
    }
    fn select_workspace(&mut self, wi: usize) {
        for disp in &self.displays {
            if disp.1 == wi {
                self.workspaces[wi].select();
                return;
            }
        }
        //the given workspace is NOT active
        //find which monitor is active
        let mi = self.determine_active_monitor();
        lower_frame(&mut self.workspaces[self.displays[mi].1]);
        self.displays[mi].1 = wi;
        resize_frame(self.displays[mi].0, &mut self.workspaces[wi]);
        self.workspaces[wi].select()
    }
    fn move_to_workspace(&mut self, mi: usize) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        let p_display = self.determine_active_monitor();
        if let Some(indexes) = self.find_parent(hwnd) {
            let f: &mut Frame = &mut self.workspaces[indexes[0]];
            WM::remove_walk(&indexes[1..], f);
        }
        self.workspaces[mi].clear_dead();
        add_window(hwnd, &mut self.workspaces[mi]);
        self.workspaces[self.displays[p_display].1].select();
    }
    /**
     * A poor mans fullscreen
     * Clear a workspace of all other windows,
     * then move the current window to that workspace
     */
    fn workspace_only(&mut self, wi: usize) {
        let hwnd = unsafe {winuser::GetForegroundWindow()};
        if let Some(indexes) = self.find_parent(hwnd) {
            let f: &mut Frame = &mut self.workspaces[indexes[0]];
            WM::remove_walk(&indexes[1..], f);
        }
        self.workspaces[wi].children.clear();
        self.workspaces[wi].children.push(Child::Window(hwnd));
        self.workspaces[wi].lsi = 0;
    }
    /**
     * Remove all windows from management without changing state
     * Should assist in preventing restarts
     * Also should enable of using the software to arrange windows
     * without using it to manage windows.
     * Because the alt tab habit can be a hard one to break.
     */
    fn clear_state(&mut self) {
        for i in 0..self.workspaces.len() {
            self.workspaces[i].children = Vec::new();
        }
    }
    /**
     * Remove only the dead windows
     * This is a hackish solution to an ugly problem
     * As far as I'm aware there is no way to be notified when a window closes.
     * This function, or a variant could just be run on every operation,
     * but seems excessive when most window closures should happen through the
     * (as of yet unfixed) close_active_window function
     * Instead, it can just be exposed as a shortcut to the user
     */
    fn clear_dead(&mut self) {
        let active_win = unsafe {winuser::GetForegroundWindow()};
        for workspace in &mut self.workspaces {
            workspace.clear_dead();
        }
        for display in &self.displays {
            let f = &mut self.workspaces[display.1];
            resize_frame(f.area, f);
        }
        activate_window(active_win);
    }
}
fn mid_point(frame: &Frame, path: &[usize], dir: Layout) -> f64 {
    if path.len() == 1 {
        if frame.layout == dir {
            return (path[0] as f64)/(frame.children.len() as f64) + 0.5;
        } else {
            return 0.5;
        }
    } else {
        if let Child::Frame(ref f) = frame.children[path[0]] {
            return (path[0] as f64 + mid_point(f,&path[1..],dir))/(frame.children.len() as f64);
        } else {
            unreachable!();
        }
    }
}
use std::fmt;
impl fmt::Debug for Child {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Child::Frame(frame) => write!(f, "{:?}", frame),
            Child::Window(w) => {
                let mut wname: Vec<u16> = Vec::with_capacity(64);
                let l = unsafe {winuser::GetWindowTextW(*w,wname.as_mut_ptr(), 64)};
                if l != 0 {
                    unsafe {wname.set_len(l as usize);}
                    match String::from_utf16(&wname) {
                        Err(_e) => write!(f, "window"),
                        Ok(s) => write!(f, "{}", s),
                    }
                } else {
                    write!(f, "Dead Window")
                }
            },
        }
    }
}

impl fmt::Debug for Frame {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.children)
    }
}

fn activate_window(hwnd: windef::HWND) {
    unsafe {
        if winuser::AllowSetForegroundWindow(winuser::ASFW_ANY) == 0 {
            println!("AllowSetForeground failed {:?}",errhandlingapi::GetLastError());
        }

        if winuser::SetForegroundWindow(hwnd) == 0 {
            println!("SetForeground failed {:?}",errhandlingapi::GetLastError());
            //Likely caused by window being invalid.
            //The ideal case is to initiate a clean of dead windows,
            //But there is no easy way to do this without major restructuring
            //The immediate two 
        }
    };
}

fn list_monitors() -> Vec<windef::RECT> {
    use std::ptr::null_mut;
    let mut v: Vec<windef::RECT> = Vec::new();
    unsafe {
        winuser::EnumDisplayMonitors(null_mut(), null_mut(), Some(monitor_callback),&mut v as *mut _ as minwindef::LPARAM);
    }
    //v[0].bottom-=39;//account for task bar
    v
}

#[no_mangle]
pub unsafe extern "system" fn monitor_callback(_hm: windef::HMONITOR, _hdc: windef::HDC,
                                               lprc: windef::LPRECT, lp: minwindef::LPARAM) -> minwindef::BOOL {
    let v = lp as *mut Vec<windef::RECT>;
    let r = *lprc;
    (*v).push(r);
    println!("Found monitor with bounds {} {} {} {}", r.left, r.top, r.right, r.bottom);
    1
}

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

enum Child {
    Window(windef::HWND),
    Frame(Frame),
}
impl Child {
    fn select(&self) -> bool {
        match self {
            Child::Window(w) => {
                if unsafe {winuser::IsWindow(*w) != 1} {
                    true
                } else {
                    activate_window(*w);
                    false
                }

            },
            Child::Frame(ref f) => match f.last_selected() {
                Some(s) => if s.select() {
                    //s is dead and needs to be culled
                    //but we're still taking self as immutable.
                    //this will need fixing later
                    f.children.len() == 0
                } else {
                    false
                }
                None => true,
            }
        }
    }
    /*assume frame
     * Given a child, unwrap it to a frame under the assumption it must be a frame
     */
    fn a_f(&self) -> &Frame {
        match self {
            Child::Frame(ref f) => f,
            Child::Window(_) => panic!("Incorrectly assumed Child to be frame"),
        }
    }
    fn a_f_mut<'a>(&'a mut self) -> &'a mut Frame {
        match self {
            Child::Frame(ref mut f) => f,
            Child::Window(_) => panic!("Incorrectly assumed Child to be frame"),
        }
    }
}
#[derive(PartialEq)]
enum Layout {
    Vertical,
    Horizontal,
}

struct Frame {
    layout: Layout,
    children: Vec<Child>,
    lsi: usize,
    area: windef::RECT,
    preserve_area: bool
}
impl Frame {
    fn last_selected(&self) -> Option<&Child> {
        match self.children.get(self.lsi) {
            Some(c) => Some(c),
            None => self.children.get(0)
        }
    }
    fn select(&self) {
        if let Some(s) = self.last_selected() {
            s.select();
        }
    }
    fn update_lsi(&mut self, path: &[usize], nsi: usize) {
        if path.len() == 0 {
            self.lsi = nsi;
        } else {
            self.children[path[0]].a_f_mut().update_lsi(&path[1..], nsi);
        }
    }
    fn clear_dead(&mut self) -> bool {
        let mut i = 0;
        while i < self.children.len() {
            let del = match self.children[i] {
                Child::Frame(ref mut f) => f.clear_dead(),
                Child::Window(w) => unsafe {winuser::IsWindow(w) != 1},
            };
            if del {
                self.children.remove(i);
            } else {
                i+=1
            }
        }
        return self.children.len() == 0;
    }
}
fn add_window(window: windef::HWND, frame: &mut Frame) {
    //just add to end of stack for now
    frame.children.push(Child::Window(window));
    resize_frame(frame.area, frame);
}
//borowing a feature from spectrwm, do a search for a window with the given title
//Probably will later update to return a Vec of possible matches
fn list_windows() -> (Vec<Vec<u16>>,Vec<windef::HWND>) {
    let mut handles: Vec<windef::HWND> = Vec::new();
    unsafe {winuser::EnumWindows(Some(window_enum_proc), &mut handles as *mut _ as minwindef::LPARAM);}
    let mut names: Vec<Vec<u16>> = Vec::new();
    for i in 0..handles.len() {
        let mut wname: Vec<u16> = Vec::with_capacity(64);
        let l = unsafe {winuser::GetWindowTextW(handles[i],wname.as_mut_ptr(), 64)};
        unsafe {wname.set_len(l as usize);}
        names.push(wname);
    }
    (names, handles)
}
fn list_programs() -> (Vec<String>, Vec<Box<std::path::PathBuf>>) {
    use std::fs::read_dir;
    use std::path::{Path,PathBuf};
    use std::env;
    let mut names: Vec<String> = Vec::new();
    let mut paths: Vec<Box<PathBuf>> = Vec::new();
    let d1 = Path::new(&env::var("USERPROFILE").unwrap()).join("Desktop");
    //rust doesn't seem to join drives names correctly,
    //so we're manually fixing things here
    let d2 = Path::new(&env::var("HOMEDRIVE").unwrap()).join("\\Users")
        .join("Public").join("Desktop");
    for entry in read_dir(d1).unwrap() {
        let entry = entry.unwrap();
        let name = entry.file_name().into_string().unwrap();
        if name.ends_with(".lnk") {
            names.push(name[..name.len()-4].to_string());
            paths.push(Box::new(entry.path()));
        }
    }
    for entry in read_dir(d2).unwrap() {
        let entry = entry.unwrap();
        let name = entry.file_name().into_string().unwrap();
        if name.ends_with(".lnk") {
            names.push(name[..name.len()-4].to_string());
            paths.push(Box::new(entry.path()));
        }
    }
    (names, paths)
}
#[no_mangle]
pub unsafe extern "system" fn window_enum_proc(hwnd: windef::HWND,
                                               lp: minwindef::LPARAM) -> minwindef::BOOL {
    let v = lp as *mut Vec<windef::HWND>;
    if winuser::IsWindowVisible(hwnd) == 1 {
        (*v).push(hwnd);
    }
    1
}

fn chunk_region(r: windef::RECT, nv: i32, nh: i32) -> Vec<windef::RECT> {
    let mut v: Vec<windef::RECT> = Vec::new();
    if nh == 0 || nv == 0 {
        v.push(r);
        return v;
    }
    let w = (r.right-r.left)/nh;
    let mut rx = (r.right-r.left) - w*nh;
    let h = (r.bottom-r.top)/nv;
    let mut ry = (r.bottom-r.top) - h*nv;
    let mut left = r.left;
    for _xi in 0..nh {
        let right = if rx>0 {
            rx-=1;
            left+w
        } else {
            left+w-1
        };
        let mut top = r.top;
        for _yi in 0..nv {
            let bottom = if ry>0 {
                ry-=1;
                top+h
            } else {
                top+h-1
            };
            v.push(windef::RECT{left: left, top: top, right: right, bottom: bottom});
            top = bottom+1;
        }
        left = right+1;
    }
    v
}
fn chunk_rigid(r: windef::RECT, frame: &Frame) -> Vec<windef::RECT> {
    print!("base {} {} {} {}\n",r.top,r.bottom,r.left,r.right);
    let mut dyn_len = match frame.layout {
        Layout::Vertical => r.bottom-r.top+1,
        Layout::Horizontal => r.right-r.left+1,
    };
    let mut dyn_count = frame.children.len();
    for c in &frame.children {
        if let Child::Frame(sf) = c {
            if sf.preserve_area {
                dyn_len -= match frame.layout {
                    Layout::Vertical => sf.area.bottom-sf.area.top+1,
                    Layout::Horizontal => sf.area.right-sf.area.left+1,
                };
                dyn_count -= 1;
            }
        }
    }
    let mut top = r.top;
    let mut left = r.left;
    let dyn_step = dyn_len/(dyn_count as i32);
    let mut dyn_rem = dyn_len-(dyn_count as i32)*dyn_step;
    let mut v: Vec<windef::RECT> = Vec::new();
    for c in &frame.children {
        if let Child::Frame(sf) = c {
            if sf.preserve_area {
                let (nr,nb) = match frame.layout {
                    Layout::Vertical => (r.right,
                                         top+sf.area.bottom-sf.area.top),
                    Layout::Horizontal => (left+sf.area.right-sf.area.left,
                                           r.bottom),
                };
                v.push(windef::RECT{left: left, top: top, right: nr, bottom: nb});
                match frame.layout {
                    Layout::Vertical => top = nb+1,
                    Layout::Horizontal => left = nr+1,
                }
                continue;
            }
        }
        let step = if dyn_rem > 0 {
            dyn_rem -= 1;
            dyn_step+1
        } else {
            dyn_step
        };
        match frame.layout {
            Layout::Vertical => {
                v.push(windef::RECT{left: left, top: top,
                                    right: r.right, bottom: top+step});
                top += step+1;
            },
            Layout::Horizontal => {
                v.push(windef::RECT{left: left, top: top,
                                    bottom: r.bottom, right: left+step});
                left += step+1;
            }
        }
    }
    for r in &v {
        print!("{} {} {} {}\n",r.top,r.bottom,r.left,r.right);
    }
    v
}
fn resize_frame(area: windef::RECT, frame: &mut Frame) {
    frame.area = area;
    let len = frame.children.len();
    let mut rigid = false;
    for c in &frame.children {
        if let Child::Frame(sf) = c {
            if sf.preserve_area {
                rigid = true;
                break;
            }
        }
    }
    let locs = if rigid {
        chunk_rigid(area, frame)
    } else {
        match frame.layout {
            Layout::Vertical => chunk_region(area, len as i32, 1),
            Layout::Horizontal => chunk_region(area, 1, len as i32),
        }
    };
    for i in 0..len{
        match &mut frame.children[i] {
            Child::Window(w) => resize_window(*w,locs[i as usize]),
            Child::Frame(ref mut f) => resize_frame(locs[i as usize],f),
        }
    }
}
fn resize_window(window: windef::HWND, area: windef::RECT) {
    //Some shenanigans to account for windows with a sizebox
    unsafe {
        let style = winuser::GetWindowLongA(window, winuser::GWL_STYLE);
        if (style as u32 & winuser::WS_SIZEBOX) == winuser::WS_SIZEBOX {
            winuser::SetWindowPos(window, winuser::HWND_TOP, area.left-7, area.top, area.right+15-area.left,
                                  area.bottom+8-area.top, winuser::SWP_NOACTIVATE);
        } else {
            //seems to be the best way to raise a window
            winuser::SetForegroundWindow(window);
            //winuser::SetWindowPos(window, winuser::HWND_TOPMOST, area.left, area.top, area.right-area.left+1,
            //                      area.bottom-area.top+1, 0);
            winuser::SetWindowPos(window, winuser::HWND_TOP, area.left, area.top, area.right-area.left+1,
                                  area.bottom-area.top+1, 0);
            //winuser::SetWindowPos(window, winuser::HWND_TOP, area.left, area.top, area.right-area.left,
            //                      area.bottom-area.top, 0);
        }
    }
}
/**
 * Tries to create a named mutex.
 * If this fails, the program terminates.
 * This named mutex is automatically created by windows,
 * and is automatically cleaned up at program termination
 */
fn ensure_single_instance() {
    use std::ptr::null_mut;
    let mut m_name = "solarwm".encode_utf16().collect::<Vec<_>>();
    m_name.push(0);
    unsafe {
        winapi::um::synchapi::CreateMutexW(null_mut(), 1, m_name.as_ptr())
    };
    if unsafe {errhandlingapi::GetLastError()} == 183 { //ERROR_ALREADY_EXISTS
        println!("Handle already exists");
        std::process::exit(1);
    }
}
fn lower_frame(frame: &mut Frame) {
    for i in 0..frame.children.len() {
        match &mut frame.children[i] {
            Child::Window(w) =>
                unsafe {winuser::SetWindowPos(*w,winuser::HWND_BOTTOM,0,0,0,0,winuser::SWP_NOACTIVATE
                                              |winuser::SWP_NOMOVE|winuser::SWP_NOSIZE);}
            Child::Frame(ref mut f) => lower_frame(f),
        }
    }
}
//toggles the title bar decorations on the top of windows
//For now, it also removes the sizebox.
//This is mostly to test if it improves spacing
fn toggle_decorations() {
    let hwnd = unsafe {winuser::GetForegroundWindow()};
    let style = unsafe {winuser::GetWindowLongA(hwnd, winuser::GWL_STYLE)};
    if (style as u32 & (winuser::WS_CAPTION | winuser::WS_SIZEBOX)) != 0 {
        let ns = style & (!winuser::WS_CAPTION & !winuser::WS_SIZEBOX) as i32;
        unsafe {winuser::SetWindowLongA(hwnd, winuser::GWL_STYLE, ns)};
    } else {
        unsafe {winuser::SetWindowLongA(hwnd, winuser::GWL_STYLE, style | winuser::WS_CAPTION as i32)};
    }
    unsafe {winuser::SetWindowPos(hwnd, winuser::HWND_TOP, 0, 0, 0, 0, winuser::SWP_NOMOVE |
                                  winuser::SWP_NOSIZE | winuser::SWP_NOZORDER | winuser::SWP_FRAMECHANGED)};
}
#[link(name = "cmenu")]
extern {
    pub fn choose_string(choices: *const *const std::os::raw::c_char,
                         count: u32, x: i32, y: i32, w: i32) -> u32;
}
